import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest} from '@angular/common/http';
import { Componente } from 'src/app/interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  private urlActivos = '/activos/activo';
  private urlConsulta = '/activos/consulta';

  constructor(private http: HttpClient) { }

  getMenuOpts(){
    return this.http.get<Componente[]>('/assets/data/menu.json');
  }

  postDatosfijos(postData, Headers) {


    return this.http.post(this.urlActivos, postData, Headers).subscribe(data => {
      console.log(data);
     }, error => {
      console.log(error);
    });
 }

   getDatosfijos() {
    return this.http.get(`${this.urlConsulta}/?status=Success`);
}

}
